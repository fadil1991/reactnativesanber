import React from 'react'
import { StyleSheet, Button, Text, Touchable, TouchableOpacity, View } from 'react-native'

export default function LoginScreen ({navigation}){
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
          title="Login Screen"
          onPress={() => navigation.navigate('LoginScreen')}
        />
  
        <Button
          color="red"
          title="Menuju Skill Screen"
          onPress={() => navigation.navigate('Drawer')}
        />
  
      </View>
        
    )
}



const styles = StyleSheet.create({})
