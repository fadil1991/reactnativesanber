import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Component from './Latihan/Component';
import YoutubeUi from './Tugas/Tugas12/App';
import Login from './Tugas/Tugas13/LoginScreen';
import About from './Tugas/Tugas13/About';
import Life from './Contoh/Lifecycle ';
import Note from './Tugas/Tugas14/App';

export default function App() {
  // const App = () => {
  return (
  //  <Component />
  // <YoutubeUi />
  // <Login />
  // <About />
  <Note />
  // <Life />
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
  );
}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
// export default App;
