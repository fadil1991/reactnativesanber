import React from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'; 
import { AntDesign } from '@expo/vector-icons'; 

const About = () => {
    return (
        <ScrollView>
        <View style={styles.body}>
            <Text style={styles.ts}>Tentang saya</Text>
            <MaterialIcons style={styles.user} name="account-circle" size={24} color="#3EC6FF" />
            <Text style={styles.nama}>Fadil Rahmat</Text>
            <Text style={styles.hobi}>programer</Text>
           
            <View>
            <Text style={styles.porto}>Portofolio</Text>
                <View style={styles.kotak}>
                <AntDesign style={styles.logo} name="gitlab" size={24} color="#3EC6FF" />
                <AntDesign style={styles.logo} name="github" size={24} color="#3EC6FF" />
                
                </View>
                <View style={styles.kotak2}>
                <Text style={styles.namaakun}>@fadil1991</Text>
                <Text style={styles.namaakun}>@FadilR</Text>
                </View>
            </View>
            <View style={styles.hub}>
            <Text style={styles.porto}>Hubungi Saya</Text>
                <View style={styles.kotak}>
                <AntDesign style={styles.logo} name="facebook-square" size={24} color="#3EC6FF" />
                </View>
                <View style={styles.kotak3}>
                <Text style={styles.namaakun}>FadilR</Text>
                </View>
                <View style={styles.kotak3}>
                <AntDesign style={styles.logo} name="twitter" size={24} color="#3EC6FF" />
                </View>
                <View style={styles.kotak3}>
                <Text style={styles.namaakun}>@fadil91</Text>
                </View>
            </View>
        </View>
        </ScrollView>
    )
}

export default About

const styles = StyleSheet.create({
    body:{
        marginTop: 64
    },
    ts:{
        fontSize: 30,
        textAlign:'center',
        fontWeight:'bold',
        marginBottom: 10
    },
    user:{
        fontSize:70,
        textAlign :'center'
    },
    nama:{
        fontSize:20,
        textAlign:'center',
        marginTop:10
    },
    hobi:{
    fontSize:15,
    color: '#3EC6FF',
    textAlign:'center',
    marginTop:10
    },
    kotak:{
        borderTopWidth:2,
        borderTopColor:"#003366",
        marginBottom:10,
        flexDirection:'row',
        justifyContent:'space-around'
    },
    kotak2:{
        marginTop:10,
        borderTopColor:"#003366",
        flexDirection:'row',
        justifyContent:'space-around'
    },
     kotak3:{
        marginTop:10,
        borderTopColor:"#003366",
        flexDirection:'row',
        justifyContent:'space-around'
    },
    porto:{
        marginTop:10,
        fontSize:20,
        marginLeft:10,
        fontWeight:'bold'
        
    },
    logo:{
        marginTop:15,
        fontSize:40
    },
    namaakun:{
        fontSize:18
    },
 
 
})
