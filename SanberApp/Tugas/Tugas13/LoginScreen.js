import React from 'react'
import { Image, 
        Platform,
        TouchableOpacity,
        StyleSheet,
        Text, 
        TextInput,
        KeyboardAvoidingView,
        View
     } from 'react-native'

const Register = () => {
    return (
        <KeyboardAvoidingView>
            
        <View style={styles.containerView} >
            <Image source={require('../Tugas13/assets/logonya.png')} />
            <Text style={styles.login}>login </Text>
            <View style={styles.formInput}>
                <Text style={styles.formtext}>Username</Text>
                <TextInput style={styles.input} />
            </View>
            <View style={styles.formInput}>
                <Text style={styles.formtext}>Password</Text>
                <TextInput style={styles.input} secureTextEntry={true}/>
            </View>
            <View style={styles.buttonbawah}>
                <TouchableOpacity style={styles.btmasuk}>  
                    <Text style={styles.mstext}>Masuk</Text>
                </TouchableOpacity>
                <Text style={styles.autotext}>atau</Text>
                 <TouchableOpacity style={styles.btreg}>  
                    <Text style={styles.daftext}>Daftar</Text>
                </TouchableOpacity>
            </View>
        </View>
        </KeyboardAvoidingView>

    )
}

export default Register

const styles = StyleSheet.create({
    ccontainerView: {
        flex :1,
        backgroundColor:'yellow'
    }, 
    formInput :{
        marginHorizontal: 30,
        marginVertical :5,
        alignContent : 'center',
        width :294
    },
    formtext:{
        color:'#003366'
    },
    login:{
        fontSize: 24,
        marginTop :63,
        textAlign: 'center',
        color: '#003366',
        marginVertical: 20

    },
    autotext:{
        fontSize: 20,
        color :'#3EC6FF',
        textAlign: 'center',
        marginBottom:10
    },
    input:{
        height: 40,
        borderColor: '#003366',
        padding:10,
        borderWidth: 1
    },
    btmasuk:{
        alignItems:'center',
        backgroundColor :'#3EC6FF',
        padding: 10,
        borderRadius:16,
        marginHorizontal: 30,
        marginBottom:10,
        width:140,
        
    },
    btreg:{
        alignItems:'center',
        backgroundColor :'#003366',
        padding: 10,
        borderRadius:16,
        marginHorizontal: 30,
        marginBottom:10,
        width:140,
        
    },
    daftext:{
        color:'white'
    },
    buttonbawah:{
        alignItems:'center'
    }


})
