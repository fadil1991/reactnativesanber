// Input:


var nama ="fadil";
var peran = "Guard";
//Output:
if(nama==""){
    console.log("Nama harus diisi!");
}else{
    if(peran==""){
        console.log("Halo "+ nama+", Pilih peranmu untuk memulai game!");
    }else{

        if(peran=="Penyihir"){
            console.log("Selamat datang di Dunia Werewolf " +nama+ ", Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
       
        }
        if(peran=="Guard"){
            console.log("Selamat datang di Dunia Werewolf " +nama+ ", Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf");
       
        }
        if(peran=="Werewolf"){
            console.log("Selamat datang di Dunia Werewolf " +nama+ ", Halo "+peran+" "+nama+",Kamu akan memakan mangsa setiap malam!");
       
        }
    }
}

// // Output untuk Input nama = '' dan peran = ''
// "Nama harus diisi!"
 
// //Output untuk Input nama = 'John' dan peran = ''
// "Halo John, Pilih peranmu untuk memulai game!"
 
// //Output untuk Input nama = 'Jane' dan peran 'Penyihir'
// "Selamat datang di Dunia Werewolf, Jane"
// "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
 
// //Output untuk Input nama = 'Jenita' dan peran 'Guard'
// "Selamat datang di Dunia Werewolf, Jenita"
// "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
 
// //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
// "Selamat datang di Dunia Werewolf, Junaedi"
// "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" 
var hari = 7;
var bulan = 6;
var tahun = 1991;

switch(bulan){
    case 1: bulan = 'Januari'; break;
    case 2: bulan = 'Februari'; break;
    case 3: bulan = 'Maret'; break;
    case 4: bulan = 'April'; break;
    case 5: bulan = 'Mei'; break;
    case 6: bulan = 'Juni'; break;
    case 7: bulan = 'Juli'; break;
    case 8: bulan = 'Agustus'; break;
    case 9: bulan = 'September'; break;
    case 10: bulan = 'Oktober'; break;
    case 11: bulan = 'November'; break;
    case 12: bulan = 'Desember'; 
}
var tanggal = hari +" "+ bulan +" "+ tahun;
console.log(tanggal);
