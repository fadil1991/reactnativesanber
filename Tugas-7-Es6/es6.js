//1. Mengubah fungsi menjadi fungsi arrow
// const golden = function goldenFunction(){
//   console.log("this is golden!!")
// }
console.log("===============");
console.log("    Nomor 1    ");
console.log("===============");

const golden =goldenFunction = () => {
    console.log("this is golden!!")
} 
golden()

 
console.log("===============");
console.log("    Nomor 2    ");
console.log("===============");
//2. Sederhanakan menjadi Object literal di ES6
//return dalam fungsi di bawah ini masih menggunakan object literal dalam ES5, ubahlah menjadi bentuk yang lebih sederhana di ES6.

const fungsiKu = (firstName, lastName) =>{
  return {
    firstName,
    lastName,
    fullName(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}

fungsiKu("Fadil", "Rahmat").fullName() 

 
console.log("===============");
console.log("    Nomor 3    ");
console.log("===============");


// 3. Destructuring
// Diberikan sebuah objek sebagai berikut:

const newData = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

 
const { firstName, lastName, destination, occupation} = newData;
console.log(firstName, lastName, destination, occupation)


console.log("===============");
console.log("    Nomor 4    ");
console.log("===============");
// 4. Array Spreading
// Kombinasikan dua array berikut menggunakan array spreading ES6

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combinedArray = [...west,...east]
console.log(combinedArray);



console.log("===============");
console.log("    Nomor 5    ");
console.log("===============");
// 5. Template Literals
// sederhanakan string berikut agar menjadi lebih sederhana menggunakan template literals ES6:

const planet = "earth"
const view = "glass"
var before = `Lorem  ${view} dolor sit amet,   
    consectetur adipiscing elit, ${planet} do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim
     ad minim veniam`
 
// Driver Code
console.log(before) 

