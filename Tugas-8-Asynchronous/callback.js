// call back

function readbooks(time, book, callback){
    console.log(`saya membaca buku ${book.name}`)
    setTimeout(function(){
        let sisawaktu =0
        if(time > book.timeSpent){
            sisawaktu = time - book.timeSpent
            console.log(` saya sudah membaca ${book.name}, sisa waktu saya ${sisawaktu}`)
            callback(sisawaktu)
        }else{
            console.log('waktu saya habis')
            callback(time)
        }
    }, book.timeSpent)
}

module.exports= readbooks