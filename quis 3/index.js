/***
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './file';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, ()=> App);
